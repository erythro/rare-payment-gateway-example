<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
	$date = $faker->dateTimeThisDecade;
    return [
        'name' => implode(" ", array_map("ucfirst",$faker->unique()->words(round(mt_rand(1,2)))) ),
        'on_sale' => $faker->boolean(10),
        'description' => $faker->paragraph(),
        'price' => $faker->numberBetween(10,1000)*10,
        'created_at' => $date,
        'updated_at' => $date
    ];
});

$factory->state(App\Product::class, 'onSale', function ($faker) {
    return [
        'on_sale' => true,
    ];
});
