<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('products');
});
Route::get('/products', 'ProductController@index')->name('products');

Route::post('/purchases/create', 'PaymentsController@create');
Route::post('/purchases/execute', 'PaymentsController@execute');

Auth::routes();

Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

