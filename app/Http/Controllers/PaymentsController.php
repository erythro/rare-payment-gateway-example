<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use App\Payments\PaymentCreationInterface;
use App\Payments\PaymentExecutionInterface;


class PaymentsController extends Controller
{
    //
	function __construct(){
	}
    
	/* function create(Request $request, PaymentCreationInterface $gateway){

	} */

    function execute(Request $request, PaymentExecutionInterface $gateway){

		$request->validate(['productID' => 'required']);
    	$product = Product::findOrFail(request("productID"));

    	try{
			$gateway->executePayment($request,$product);
    	} catch (Exception $e){
    		return response([ 'status' => $e->getMessage() ], 422);
    	}

		return [ 'status' => 'success' ];
    }
}
