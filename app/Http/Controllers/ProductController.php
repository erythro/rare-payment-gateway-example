<?php

namespace App\Http\Controllers;
use Auth;
use App\Product;
use Illuminate\Http\Request;


class ProductController extends Controller
{
	function __construct(){
		$this->paymentMethod = env('CHECKOUT_METHOD');
	}

    function index(){
    	return view('store',[
    		'products' => Product::orderBy('created_at','desc')->paginate(12),
    		'count' => Product::count(),
    		'user' => Auth::user(),
    		'paymentMethod' => $this->paymentMethod
    	]);
    }
}
