<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class PaymentServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {   
        if(env('CHECKOUT_METHOD')=="Stripe"){
            $this->app->bind('App\Payments\PaymentExecutionInterface', 'App\Payments\StripeGateway');
        }
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
