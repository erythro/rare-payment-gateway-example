<?php
namespace App\Payments;

use App\Product;
use Illuminate\Http\Request;

Interface PaymentCreationInterface{
	function createPayment(Request $request,Product $product);
}