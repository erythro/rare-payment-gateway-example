<?php
namespace App\Payments;

use App\Product;
use Illuminate\Http\Request;

Interface PaymentExecutionInterface{
	function executePayment(Request $request,Product $product);
}