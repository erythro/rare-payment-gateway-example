<?php
namespace App\Payments;

use App\Payments\PaymentExecutionInterface;
use App\Product;
use Illuminate\Http\Request;
use Stripe\{Stripe, Charge};
class StripeGateway implements PaymentExecutionInterface{

	function __construct(){
		Stripe::setApiKey(config("services.stripe.secret"));
	}

	function executePayment(Request $request, Product $product){
		$charge = Charge::create([
			'amount' => $product->price,
			'currency' => 'gbp',
			'source' => request("token")]);
	}
	
}