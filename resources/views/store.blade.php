@extends('layouts.app')

@section('content')
    <div class="container">
        <section class="section">
            <div class="columns is-mobile is-8 is-variable is-multiline is-centered">
                @foreach ($products as $product)
                    <div class="product column is-four-fifths-mobile is-half-tablet is-one-third-desktop">
                        <div class="box">
                            <figure class="image">
                                <img class="is-rounded is-fit-cover" src="https://via.placeholder.com/400x150">
                            </figure>
                            <div class="content">
                                <h3 class="title has-text-centered">{{$product->name}}</h3>
                                @if($product["on_sale"])
                                    <p class="has-text-centered">
                                        <span class="tag is-primary is-rounded">On Sale</span>
                                    </p>
                                @endif
                                <p class="has-text-centered">{{ $product->description }}</p>
                                @if($paymentMethod == "Stripe")
                                    <stripe-checkout :product="{{ $product }}" :user="{{ json_encode($user) }}"></stripe-checkout>
                                @endif
                            </div>
                                <p class="has-text-centered is-size-7" >Share this Product</p>
                                <p class="has-text-centered">
                                    @foreach(["facebook-f","twitter","instagram","whatsapp","youtube"] as $style)
                                        <span class="icon is-medium">
                                            <a href="#" class="has-text-grey">
                                                <span class="fa-stack">
                                                    <i class="fas fa-circle fa-stack-2x"></i>
                                                    <i class="fab fa-{{$style}} fa-stack-1x fa-inverse"></i>
                                                </span>
                                            </a>
                                        </span>
                                    @endforeach
                                </p>
                        </div>
                    </div>
                @endforeach
            </div>
                {{ $products->links("vendor.bulmapagination") }}
        </section>
    </div>
@endsection

@section('scripts')
    @if($paymentMethod == "Stripe")
        <script src="https://checkout.stripe.com/checkout.js" type="text/javascript"></script>
    @endif
@endsection